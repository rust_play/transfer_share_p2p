let transferFile;
let log = console.log;
const wsUri =
  ((window.location.protocol == "https:" && "wss://") || "ws://") +
  window.location.host +
  "/ws";

conn = new WebSocket(wsUri);
console.log(conn)
log("Connecting...");
conn.onopen = function () {
  log("Connected.");
};

//On Message
conn.onmessage = async function (e) {  

  let buffer = await e.data.arrayBuffer()  
  let message = arrayBufferToString(buffer)
  console.log(message)

  if (message.includes("Receiver Browser - Loader")){
    const loader = document.createElement("div");
    loader.classList.add("loader");
    loader.setAttribute("id","loader");
    const element = document.getElementById("sendFileWS");
    element.textContent = "Receiving File"
    element.appendChild(loader);
    return
  }

  if ( message.includes("File has been saved on desktop. Code expired. Refresh site & get new code") ||
       message.includes("File transfer complete. Request new code for next transfer")) {
       message_array = message.split("."); 
       for (var i = 0; i < message_array.length; i++) {
        console.log(message_array[i]);
        document.getElementById("transfer_finished").textContent = document.getElementById("transfer_finished").textContent + "\n" + message_array[i]
      }

      let remove_loader = document.getElementById("loader");
      if (remove_loader){
        remove_loader.remove();
      }
      document.getElementById("connection_code").value = ""
      const element = document.getElementById("sendFileWS");
      element.textContent = "Send File"
      get_file_list();
     
  } else {
    document.getElementById("transfer_code").textContent = document.getElementById("transfer_code").textContent + "\n" + message 
  }

};

//On Send Method
async function sendFileWS() {
  document.getElementById("transfer_finished").textContent = ""
  if (typeof transferFile == 'undefined'){
    alert("Select file for UDP transfer")
    return 
  }

  if ((!document.getElementById("connection_code").value)){
    alert("Missing UDP Code")
    return
  }

  let arrayBuffer = await transferFile[0].arrayBuffer();
  var uintArray = new Uint8Array(arrayBuffer);
  var array = Array.from(uintArray)


  const loader = document.createElement("div");
  loader.classList.add("loader");
  loader.setAttribute("id","loader");
  const element = document.getElementById("sendFileWS");
  element.appendChild(loader);

  console.log(transferFile[0].type);
  //On Send WS 
  conn.send(
    JSON.stringify({
      file: array,
      file_type: transferFile[0].type,
      connection_code: document.getElementById("connection_code").value
    }))
}

//On Close
conn.onclose = function () {
  log("Disconnected.");
  conn = null;
};

let shareFileBtn = document.getElementById('shareFile');
let shareFileExecBtn = document.getElementById('shareFileExec');
let sendFileRestBtn = document.getElementById('sendFileRest');
let sendFileWSBtn = document.getElementById('sendFileWS');
let sendTestDataBtn = document.getElementById('sendTestButton');
let getTestDataBtn = document.getElementById('getTestButton');
let copyBtn = document.getElementById('copy');

var fileToRead = document.getElementById("inputFile");


if (fileToRead != null) {
  fileToRead.addEventListener("change", function(event) {
    var files = fileToRead.files;
    if (files.length) {
        files.Uint8Array;
        console.log("Filename: " + files[0].name);
        console.log("Type: " + files[0].type);
        console.log("Size: " + files[0].size + " bytes");
        console.log("Path: " + files[0].l + " bytes");
        
    }
    transferFile = event.target.files  
    console.log(transferFile[0])
}, false);
} 


async function shareFile() {
  window.open("/p2p/", '_blank').focus();
}

 async function sendFileRest() {
   if (transferFile && transferFile[0]) {
    let objectUrl = (window.URL || window.webkitURL).createObjectURL(transferFile[0]);
    response = await fetch(objectUrl).then(data => {
      // Work with JSON data here
      console.log("data")
      console.log(data);
      return data
    }).catch(err => {
      //alert(err)
      return null
    });
    
    const form = new FormData();
    form.append("file", transferFile[0]);
    form.append("file_type", transferFile[0].type);
    form.append("connection_code", document.getElementById("connection_code").value);
    let x1 = await fetch('http://localhost:8080/upload/', {
        method: 'POST',
        body: form
    }).then( data => {
      console.log(data)
    }) } else {
      alert ("Select file for UDP transfer")
    }
}

async function send_test_data() { 
  await fetch('http://localhost:8080/v1/set_test', {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          'name': 'item-one',
          'quantity': 1
      })
  }).then( async data => {
    console.log(data)
  })
}

async function get_test_data() { 
  window.open('http://localhost:8080/v1/get_test','_blank');
}

async function get_file_list() { 

  await fetch('http://localhost:8080/v1/get-file', {
    method: 'GET',
  }).then( async data => {
    
      listElement = document.getElementById("list");
      while (listElement.children.length > 0) {
        for (const child of listElement.children) {
          listElement.removeChild(child)
        }
      }

      let body = await data.json()
      console.log(body);
      for(var i = 0; i < body.length; i++) {
        var option = document.createElement("button");
        option.innerHTML = body[i];
        option.style = "line-height: 20px; border: none; color: white; font-weight: bold; background: grey; margin-top: 5px; margin-bottom: 5px; cursor: pointer; padding-bottom: 5px; width: 100%; border-radius: 7.5px;"
        option.onclick = function( ) {
          window.open("/file_preview/".concat(this.outerText))
        } 
        document.getElementById("list").appendChild(option);
      }
  }) 
  
}

async function copy_to_clipboard() {
  await navigator.clipboard.writeText(document.getElementById("transfer_code").innerHTML.trim());
  alert("Copied the text: " + document.getElementById("transfer_code").innerHTML);
}

 function init() {

  get_file_list();

  sendFileWSBtn.addEventListener("click", (e) => {
    e.preventDefault();
    this.sendFileWS();
  })

  shareFileBtn.addEventListener("click", (e) => {
    e.preventDefault();
    this.shareFile();
  })

  sendFileRestBtn.addEventListener("click", (e) => {
    e.preventDefault();
    this.sendFileRest();
  })

  sendTestDataBtn.addEventListener("click", (e) => {
    e.preventDefault();
    this.send_test_data();
  })

  getTestDataBtn.addEventListener("click", (e) => {
    e.preventDefault();
    this.get_test_data();
  })

  copyBtn.addEventListener("click", (e) => {
    e.preventDefault();
    this.copy_to_clipboard();
  })
  }


function arrayBufferToString(buffer) {
  var arr = new Uint8Array(buffer);
  var str = String.fromCharCode.apply(String, arr);
  if(/[\u0080-\uffff]/.test(str)){
      throw new Error("Error - still encoded multibytes");
  }
  return str;
}

init()
