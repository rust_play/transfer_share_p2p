# Holepunch + UDP DataTransfer example
1. For MacOs users: increase datagram size from 9216 to 65535 bytes: sudo sysctl -w net.inet.udp.maxdgram=65535

2. Run http://127.0.0.1:8080 in two separate clients.
Copy partner (destination) code.
Upload File.
Press "Send File" for UDP transfer.

3. Check Console Log

# LibP2P example 
0. Turn OFF VPN if used

1. Run http://localhost:8080/p2p/ Start sharing file

2. Open http://localhost:8080/p2p/ in other client. Share file

3. Check Console Log
