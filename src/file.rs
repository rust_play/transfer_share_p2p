pub mod file_process {

    use std::str::from_utf8;
    use std::time::{Duration, SystemTime};
    use std::sync::{Arc, Mutex};
    use std::collections::HashMap;
    use dirs::home_dir;
    use tokio::sync::mpsc::Receiver;

    use crate::packet::packet::{File, DataPacket};
    use crate::socket_server::process_server::UdpServer;
    extern crate futures;
    extern crate websocket;

    pub async fn process_incoming_message(mut receiver: Receiver<DataPacket>, server: UdpServer, tx: tokio::sync::mpsc::UnboundedSender<Result<warp::filters::ws::Message, warp::Error>>) {

        let incoming_file_list = Arc::new(Mutex::new(HashMap::new()));
        let cloned_message_list = Arc::clone(&incoming_file_list);
        let socket_cloned = Arc::clone(&server.socket);

        let drop_packet_testing_only: u32 = std::convert::TryInto::try_into(rand::Rng::gen_range(&mut rand::thread_rng(), 0..5)).unwrap();

        //Packet Receiver
        let _hande2 = tokio::task::spawn(async move {    
            let mut interval = tokio::time::interval(Duration::from_secs(1));
           
            loop {

                interval.tick().await;
                println!("Receiving Packets");

                let mut file_extension: String;
                let packet = receiver.recv().await.unwrap();
                let mut thread_messages = cloned_message_list.lock().unwrap();
                let file = thread_messages.entry(packet.unique_id).or_insert(File::new(packet.unique_id));
                
                file.add_packet(packet, drop_packet_testing_only);

                if file.complete {
                   file.sort();
                   println!("Successfully transferred in message {:?}", file.unique_id);

                   let packet = file.packets.iter().last().unwrap();

                   let mut fe: Vec<u8> = Vec::new();
                   fe.push(packet.file_ext_0);
                   fe.push(packet.file_ext_1);
                   fe.push(packet.file_ext_2);
                   fe.push(packet.file_ext_3); 

                   file_extension = from_utf8( fe.as_ref() ).unwrap().to_string();
                   
                   file_extension = file_extension.replace(" ", ""); 

                   let id_bytes0: [u8; 4];
                   id_bytes0 = [255, 255, 255, 255];
                   let udp_socket_test = server.socket.clone();

                      tokio::task::spawn(async move {
                        let _buf =  [0u8; 4];
                              match udp_socket_test.send( &id_bytes0).await {
                                Ok(_usize) => {
                                    println!("Send [closing-resend] message");
                                },
                                Err(_e) => {
                                    //println!("Error receiving - pp: {}", e);
                                }
                            } 
                            drop(udp_socket_test);
                    });  
                    
                   let mut file_to_save: Vec<u8> = Vec::new();
                    for (_i, element) in file.packets.iter().enumerate() {
                        for (_i, el) in element.chunk_data.iter().enumerate(){
                            //println!("element, {:?}", el);
                            if _i as u32  == element.chunk_offset as u32 + element.chunk_size as u32 {
                                 //println!("last element")
                            }
                            file_to_save.push(*el);
                        }
                    }
                    
                    std::thread::sleep(std::time::Duration::from_secs(5));    
                    let bin_message: warp::ws::Message = warp::ws::Message::binary("File has been saved on desktop. Code expired. Refresh site & get new code");
                    let message: Result<warp::ws::Message, warp::Error> = Ok(bin_message);
                    let _ = tx.send(message);

                    let _handle = tokio::task::spawn(async move {
                        let path = home_dir().unwrap();
                        let path_str = path.to_str().unwrap();

                        let file_name = format!("{}/{}.{}", path_str, "rust_udp-".to_owned() + uuid::Uuid::new_v4().to_string().as_str() , file_extension);
                        println!("FILE, {}",  file_name);
                        tokio::fs::write(&file_name, file_to_save).await.map_err(|e| {
                            eprint!("error: writing file: {}", e);
                            warp::reject::reject()
                        }).unwrap();

                    });
                };
            }
        });

       //Packet Re-sender
        let _handle2 = tokio::task::spawn(async move {

            let mut interval = tokio::time::interval(Duration::from_secs(1));
            let sec1 = Duration::from_secs(1);  
            let sec30 = Duration::from_secs(30);  
            let sys_time_now = SystemTime::now();
            let mut new_sys_time = sys_time_now.checked_add( sec30).unwrap(); 
        
             loop {

                interval.tick().await;
                let message_list_mutex = incoming_file_list.lock().unwrap();
                if message_list_mutex.iter().count() == 0 {
                    continue;
                } else {
                   // println!("Message Count {:?}", message_list_mutex.iter().count());
                }

                let outdated = message_list_mutex.iter().filter(|m|                   
                    if new_sys_time.duration_since(m.1.last_received_at).unwrap().as_secs() > 30 {
                        return true;   
                    } else {
                        new_sys_time = new_sys_time.checked_add(sec1).unwrap(); 
                        return false;
                    }
                );

                if outdated.into_iter().count() > 0 {
                    break;
                } 
            } 
  
            let udp_socket_test = socket_cloned;
            let mut message_list_mutex = incoming_file_list.lock().unwrap();

            for (_, message) in message_list_mutex.iter_mut() {
                message.sort();
            } 

            let incomplete_message: Vec<_> = message_list_mutex.iter()
            .filter(|&(_, v)| v.complete == false)
            .map(|(_, v)| v)
            .collect();
        
            if incomplete_message.iter().count() == 0 {
                println!("All messages have been transferred");
            } else {

                println!("Not ALL packets have been transferred");
                let mut packet_bytes: [u8; 4];
                for message in incomplete_message.iter() {
                    if !message.complete {
                        println!("Missing Packet List, {:?}", message.get_missing_packet_list());
                        println!("Missing Packet List - Length, {:?}", message.get_missing_packet_list().len()); 
                        for packet in message.get_missing_packet_list() {          
                            packet_bytes = packet.to_be_bytes();
                            let socket = udp_socket_test.clone();
                             tokio::task::spawn(async move {
                                      match socket.send( &packet_bytes).await {
                                        Ok(_usize) => {
                                            //println!("Packet Resend {:?}", packet);
                                        },
                                        Err(_e) => {
                                           // println!("Error receiving - pp: {}", e);
                                        }
                                    }  
                            });
                        }   
                    }
                }
                drop(udp_socket_test);
            } 
        });
    }  
}