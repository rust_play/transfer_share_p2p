pub mod routers {

    use std::net::SocketAddr;
    extern crate num_cpus;
    use crate::hander::route_hander;
    use crate::p2p::p2p_process;

    use std::fs;
    use warp::ws::Message;
    use warp::Filter;
    use std::{collections::HashMap, sync::Arc};
    use tokio::sync::{mpsc, RwLock};
    use std::convert::Infallible;

    type Users = Arc<RwLock<HashMap<String, mpsc::UnboundedSender<Result<Message, warp::Error>>>>>;
    type Items = HashMap<String, i32>;
    type Peers = HashMap<String, String>;
    type Files = HashMap<String, String>;
    type SecretCode = Arc<RwLock<HashMap<usize, String  >>>;

    use state::Storage;
    pub static G_STORE: Storage<futures::lock::Mutex<HashMap<String, Arc<StoreObj>>>> = Storage::new();

    #[derive(Clone)]
    pub struct StoreObj {
      pub item_list: Arc<RwLock<Items>>,
      pub peer_list: Arc<RwLock<Peers>>,
      pub file_list: Arc<RwLock<Files>>
    }

    impl StoreObj {
        pub fn new() -> Self {
            StoreObj {
                item_list: Arc::new(RwLock::new(HashMap::new())),
                peer_list: Arc::new(RwLock::new(HashMap::new())),
                file_list: Arc::new(RwLock::new(HashMap::new())),
            }
        }

        pub fn get_item_list (self) -> Arc<RwLock<Items>> {
            return self.item_list;
        }

        pub fn get_peer_list (self) -> Arc<RwLock<Peers>> {
            return self.peer_list;
        }     
        
        pub fn get_file_list2 (self) -> Arc<RwLock<Files>> {
            return self.file_list;
        }    
    }
  
    pub fn create_server_with_filter(socket_address: SocketAddr) -> impl warp::Future<Output = ()> {

        let store = StoreObj::new();
        let users = Users::default();
        let code = SecretCode::default();

        let init_ws = init_ws(users, code);
        
        let static_file = static_file();
        let upload_file = upload_file();
        let download_file = download_file();
        let get_file_list= get_file_list();
        let share_file = share_file();
        let res_404n = res_404();

        //Testing
        let add_item = add_item(store.clone());
        let get_item = get_item(store.clone());

        //LibP2P
        let p2p_upload = p2p_file_upload(store.clone());
        let p2p_download = p2p_file_download(store.clone());

        //Peer LibP2P
        let get_peer_item = get_peer_item(store.clone());
        let get_peer_list = get_peer_list(store.clone());

        //Peer LibP2P
        let get_file_item = get_file_item(store.clone());
        let get_peer_file_list = get_peer_file_list(store.clone());
    
        let routes = static_file.or(get_peer_file_list).or(get_peer_item).or(get_peer_list).or(p2p_upload).or(p2p_download).or(share_file).or(add_item).or(get_item).or(init_ws).or(get_file_list).or(upload_file).or(download_file).or(get_file_item).or(res_404n);
        
        let server = warp::serve(routes).try_bind(socket_address);
        server
    }
    
    pub fn upload_file() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let upload_route = warp::post()
            .and(warp::path("upload"))
            .and(warp::path::end())
            .and(warp::multipart::form().max_length(10_000_000))
            .and_then( route_hander::upload);
        upload_route
    }

    pub fn download_file() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let download_route = warp::path("file_preview").and(warp::fs::dir(dirs::home_dir().unwrap())); 
        download_route
    }

    pub fn get_file_list() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
         let get_files = warp::get()
        .and(warp::path("v1"))
        .and(warp::path("get-file"))
        .and(warp::path::end())
        .and_then(route_hander::get_file_list);
        get_files
    }

    pub fn static_file() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let files = warp::fs::dir("./static");
        files
    }

    pub fn share_file() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let share_file = warp::path("p2p").map(|| {
             warp::http::Response::builder()
                 .body(fs::read_to_string("./static/p2p.html").expect("404 404?"))
         });
         share_file
    }

     pub fn p2p_file_upload(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let p2p_upload = warp::post()
        .and(warp::path("p2p-upload"))
        .and(warp::path::end())
        .and(warp::multipart::form().max_length(10_000_000))
        .and(with_store(store))
        .and_then(p2p_process::p2p_process::p2p_file_upload);
        p2p_upload
    }

    pub fn p2p_file_download(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let p2p_download = warp::post()
        .and(warp::path("p2p-download"))
        .and(warp::path::end())
        .and(warp::multipart::form())
        .and(with_store(store))
        .and_then(p2p_process::p2p_process::p2p_file_download);
        p2p_download
    }
    
    pub fn add_item(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let add_item = warp::post()
        .and(warp::path("v1"))
        .and(warp::path("set_test"))
        .and(warp::path::end())
        .and(route_hander::post_json())
        .and(with_store(store))
        .and_then(route_hander::insert_test_item);
        add_item
    }

    pub fn get_item(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let list = warp::get()
        .and(warp::path("v1"))
        .and(warp::path("get_test"))
        .and(warp::path::end())
        .and(with_store(store))
        .and_then(route_hander::get_test_item_list);
        list
    }

    pub fn get_peer_item(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let item = warp::post()
        .and(warp::path("peer"))
        .and(warp::path("get_item"))
        .and(warp::path::end())
        .and(with_store(store))
        .and(warp::multipart::form().max_length(10_000_000))
        .and_then(route_hander::get_peer_item);
        item
    }

    pub fn get_peer_list(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let list = warp::get()
        .and(warp::path("peer"))
        .and(warp::path("get_list"))
        .and(warp::path::end())
        .and(with_store(store))
        .and_then(route_hander::get_peer_list);
        list
    }

    pub fn get_file_item(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let item = warp::post()
        .and(warp::path("peer"))
        .and(warp::path("get_file"))
        .and(warp::path::end())
        .and(with_store(store))
        .and(warp::multipart::form().max_length(10_000_000))
        .and_then(route_hander::get_file_item);
        item
    }

    pub fn get_peer_file_list(store: StoreObj) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let list = warp::get()
        .and(warp::path("peer"))
        .and(warp::path("get_file_list"))
        .and(warp::path::end())
        .and(with_store(store))
        .and_then(route_hander::get_peer_file_list);
        list
    }

    pub fn init_ws(users: Users, secret_code: SecretCode) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let init = warp::path("ws")
        .and(warp::ws())
        .and(with_user(users))
        .and(with_code(secret_code))
        .map(|ws: warp::ws::Ws, users, secret_code: SecretCode|  ws.on_upgrade(move |socket| route_hander::ws_connect(socket, users, secret_code)));
        init
    }

    pub fn res_404() -> impl Filter<Extract = impl warp::Reply, Error = Infallible> + Clone {
        let res_404 = warp::any().map(|| {
             warp::http::Response::builder()
                 .status(warp::http::StatusCode::NOT_FOUND)
                 .body(fs::read_to_string("./static/404.html").expect("404 404?"))
         });
         res_404
     }

    fn with_user(user: Users) -> impl Filter<Extract = (Users,), Error = Infallible> + Clone {
        warp::any().map(move || user.clone())
    }

    fn with_store(store: StoreObj) -> impl Filter<Extract = (StoreObj,), Error = Infallible> + Clone {
        warp::any().map(move || store.clone())
    }

    fn with_code(code: SecretCode) -> impl Filter<Extract = (SecretCode,), Error = Infallible> + Clone {
        warp::any().map(move || code.clone())
    }
    
}


