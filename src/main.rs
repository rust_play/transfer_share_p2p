extern crate num_cpus;
use std::env;
use std::net::SocketAddr;
use client_server::route::routers; 

#[tokio::main]
async fn main() {
    //Warp Server 
    //UDP Client - Server => Handler.rs (ws_connect())
    //LibP2P http://localhost:8080/p2p/
    println!("CPU Number: {:?}", num_cpus::get());
    let addr = env::args().nth(1).unwrap_or_else(|| "127.0.0.1:8080".to_string());
    let socket_address: SocketAddr = addr.parse().expect("valid socket address");
    let server = routers::create_server_with_filter(socket_address);
    println!("Running server at {}!", addr);
    server.await;
}
