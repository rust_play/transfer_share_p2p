//Each Server is a Client
pub mod process_server {
    
    extern crate warp;
    use crate::connector::nat::NatSetGet;
    use std::io::Cursor;
    use crate::packet::packet::DataPacket;
    use crate::connector::nat;
    use tokio::{net::UdpSocket, sync::mpsc::Sender};
    use std::sync::Arc;
    use crate::file;

    const BUFFER_SIZE: usize = 65535;
    pub struct UdpServer  {
        pub socket: Arc<UdpSocket>,
        sender: Sender<DataPacket>,
        task_number: usize,
    }
 
    impl UdpServer {

        pub async fn new( sender: Sender<DataPacket>, socket: Arc<UdpSocket>, )  -> UdpServer {
            let task_num = num_cpus::get();
            UdpServer {
                socket,
                sender,
                task_number: task_num,
            }
        }

        async fn bind_server(nat_socket: Arc<tokio::net::UdpSocket>, secret_code_for_connection: String) -> Arc<tokio::net::UdpSocket> {

            let bind_addr = (std::net::Ipv4Addr::from(0 as u32), 0);
            let _socket = match tokio::net::UdpSocket::bind(bind_addr).await {
                Ok(socket) => {
                    let arc_socket = Arc::new(socket);
                    let _hp_socket = NatSetGet::holepunch(nat_socket, arc_socket, secret_code_for_connection).await;
                    return _hp_socket;
                }
                Err(e) => {panic!("error: can not bind socket: {}", e)}
            };          
        }

        pub fn start_sending(&self) {

            for _  in 0..self.task_number {
                let socket_cloned = self.socket.clone();
                let sender_cloned = self.sender.clone();
                let _handle = tokio::task::spawn(async move {
        
                    loop {
                        let mut buf = [0; BUFFER_SIZE ];
                        match socket_cloned.recv_from(&mut buf).await {
                            Ok((_usize, _socket_addr)) => {
                                let packet = DataPacket::new(&mut Cursor::new(&buf));
                                let _x = sender_cloned.send(packet).await;
                            },
                            Err(e) => {
                                println!("Error receiving: {}", e);
                            }
                        }
                    }
                });  
            };
        }
        
        pub async fn start_server(tx: tokio::sync::mpsc::UnboundedSender<Result<warp::filters::ws::Message, warp::Error>>, secret_code_for_connection: String ) {
          
            let socket;
            //Message channel
            let (sender, receiver) = tokio::sync::mpsc::channel::<DataPacket>(1000);       

            //Get Existing NAT Connection
            if self::UdpServer::has_nat_key().await {
                    let old_nat = self::UdpServer::get_nat_inside().await;
                    socket = self::UdpServer::bind_server(old_nat, secret_code_for_connection.clone()).await;
            } else {
                //Create New NAT Connection
                let arc_nat_socket = nat::NatSetGet::start_nat().await;
                //Bind Server to NAT translation
                socket = self::UdpServer::bind_server(arc_nat_socket, secret_code_for_connection.clone()).await;
            }

            //Create Server Instance
            let _udpserver = self::UdpServer::new(sender, socket).await;

            //Create Receiving Tasks; Send Message to MPSC Channel
            _udpserver.start_sending();
        
            //MPSC Channel
            file::file_process::process_incoming_message(receiver, _udpserver, tx).await;

        }

        async fn get_nat_inside() -> Arc<tokio::net::UdpSocket> {
            let nat = crate::connector::nat::G_MAP_CODE_SOCKET.get().lock().await;
            let udp_socket = nat.get("key-nat-translator").unwrap();
            let udp_socket_cloned =  udp_socket.clone();  
            return udp_socket_cloned;
        }

        async fn has_nat_key() -> bool {
        let nat = crate::connector::nat::G_MAP_CODE_SOCKET.try_get();

        match nat {
            Some(_) => { return  true }
            None => { return false; }, 
          }
       }
    }
}
