pub mod p2p_process {
    
    use crate::p2p::p2p_network::p2p_network;
    use bytes::Buf;
    use futures::{TryStreamExt, FutureExt};
    use warp::{
        multipart::FormData,
        Rejection, Reply,
    };

    use crate::route::routers::StoreObj;
    use dirs::home_dir;
    use dirs::desktop_dir;
    use std::vec::Vec;
    use libp2p::core::{Multiaddr, PeerId};
    use std::path::PathBuf;
    use std::str;
    use libp2p::multiaddr::Protocol;

    #[derive(clap::Parser, Debug)]
    #[clap(name = "libp2p file sharing example")]

    struct Opt {
        /// Fixed value to generate deterministic peer ID.
        #[clap(long)]
        secret_key_seed: Option<u8>,

        #[clap(long)]
        peer: Option<Multiaddr>,

        #[clap(long)]
        listen_address: Option<Multiaddr>,

        #[clap(subcommand)]
        argument: CliArgument,
    }
    
 
    #[derive(Debug, clap::Parser)]
    enum CliArgument {
        Provide {
            #[clap(long)]
            path: PathBuf,
            #[clap(long)]
            name: String,
        },
        Get {
            #[clap(long)]
            name: String,
        },
    }

    pub async fn p2p_file_upload(mut form: FormData, store: StoreObj) -> Result<impl Reply, Rejection> {

        while let Ok(Some(part)) = form.try_next().await {
            let filename = match part.filename() {
                Some(filename) => filename.to_string(),
                None => return Ok("None".to_string()),
            };

            println!("Receiving file: {}", filename);
    
            let mut data = Vec::new();
            let mut stream = part.stream();
            
            while let Ok(Some(mut chunk)) = stream.try_next().await {
                let slice: &[u8] = chunk.chunk();
                data.extend_from_slice(slice);
                chunk.advance(slice.len());
            }

            let seed_secret_key: Option<u8> = std::option::Option::None;  
            let path = home_dir().unwrap();
            let path_str = path.to_str().unwrap();
            let file_name = format!("{}/{}.{}", path_str, uuid::Uuid::new_v4().to_string().as_str() , filename);
            let _z = tokio::fs::write(&file_name, data).await.map_err(|e| {
                eprint!("error writing file: {}", e);
                warp::reject::reject()
            }).unwrap();

            let (mut network_client, mut network_events, network_event_loop) = p2p_network::new(seed_secret_key, store).await.unwrap();
        
            // Spawn the network task for it to run in the background.
            tokio::task::spawn(network_event_loop.run());
    
            network_client.start_listening("/ip4/0.0.0.0/tcp/0".parse().unwrap()).await.expect("Listening not to fail.");
            network_client.start_providing(file_name.clone()).await; 

            let mut network_client_clone = network_client.clone();

            loop {
                match async_std::stream::StreamExt::next(&mut network_events).await {
                    // Reply with the content of the file on incoming requests.
                    Some(p2p_network::Event::InboundRequest { request, channel }) => {
                        if request == file_name.clone() {
                            network_client_clone
                            .respond_file( std::fs::read(&file_name.clone()).unwrap() , channel)
                            .await;
                        }
                    } 
                    None => return Ok("None".to_string()),
                }
            }
        }
        Ok("success".to_string())

    }

    pub async fn p2p_file_download(mut form: FormData, store: StoreObj) -> Result<impl Reply, Rejection> {

        let _handle = tokio::task::spawn(async move {        
        let seed_key: Option<u8> = Option::None;
        let (mut network_client, _network_events, network_event_loop) = p2p_network::new(seed_key, store).await.unwrap();

        tokio::task::spawn(network_event_loop.run());

        while let Ok(Some(part)) = form.try_next().await {
            
            let info = match part.name() {
                "filename" => "filename",
                "address" => "address",
                &_ => "None",
            };

            let mut data = Vec::new();
            let mut stream = part.stream();
                
            while let Ok(Some(mut chunk)) = stream.try_next().await {
                let slice: &[u8] = chunk.chunk();
                data.extend_from_slice(slice);
                chunk.advance(slice.len());
            };

            //Peer Address
            if info == "address" {
                let address_string = str::from_utf8(&data).unwrap().to_string();
                let address = address_string.parse::<Multiaddr>().unwrap();
                let peer_multiaddress: Option<Multiaddr> = Option::Some(address);

                if let Some(addr) = peer_multiaddress {
            
                    let peer_id = match addr.iter().last() {
                
                        Some(Protocol::P2p(hash)) => {
                        
                            PeerId::from_multihash(hash).expect("Valid hash.")
                        }
                        _ =>  PeerId::random()
                    };
                    network_client
                        .dial(peer_id, addr)
                        .await
                        .expect("Dial to succeed");
                }
            }
          
            //File Name
            if info == "filename" {
                let name = str::from_utf8(&data).unwrap().to_string();
                let split_data: Vec<&str>  = name.split('.').collect();

                let providers = network_client.get_providers(name.clone()).await;
                let requests = providers.into_iter().map(|p| {
                    let mut network_client = network_client.clone();
                    let name = name.clone();
                    async move { network_client.request_file(p, name).await }.boxed()
                });
        
                // Await the requests, ignore the remaining once a single one succeeds.
                let file_content = futures::future::select_ok(requests)
                .await
                .map_err(|_| "None of the providers returned file.").unwrap()
                .0;
        
                let path_str = desktop_dir().unwrap();
                let file_name = format!("{}/{}.{}", path_str.display(), uuid::Uuid::new_v4().to_string().as_str() , split_data[2]);
                let _z = tokio::fs::write(&file_name, file_content).await.map_err(|e| {
                    eprint!("error writing file: {}", e);
                    warp::reject::reject()
                }).unwrap();
            }            
        }
    }); 
    Ok("success".to_string())
}
}