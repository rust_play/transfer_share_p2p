//Each Client is a Server
pub mod process_client {
    
    use crate::connector::nat::NatSetGet;
    use std::convert::TryFrom;
    use rand::Rng;
    use std::collections::HashMap;
    use std::convert::TryInto;
    use tokio::net::UdpSocket;
    use std::sync::Arc;

    const IP_HEADER: usize = 20;
    const UDP_HEADER: usize = 8;
    const CUSTOM_HEADER: usize = 20;
    const BUFFER_SIZE: usize = 65535 - IP_HEADER - UDP_HEADER - CUSTOM_HEADER;

    pub async fn start_client(file_vector: Vec<u8>, mut file_extension: String, holepunch_code: String, nat_socket: Arc<UdpSocket> ) {

        let client_addr: std::net::SocketAddr = ([127, 0, 0, 1],0).into();
        let socket = tokio::net::UdpSocket::bind(client_addr).await.unwrap();
        let arc_socket = Arc::new(socket);

        let mut transfer_data_storage: HashMap<u32, Vec<u8>> = HashMap::new();
        let mut chunk_offset: u32 = 0;
        let mut eof_flag: u8 = 0;
        let chunk_size: u32 = BUFFER_SIZE.try_into().unwrap();//60000;

        if file_extension.contains("video") {
            file_extension = "video/mov".to_owned();
        }
        let file_ext: Vec<&str> = file_extension.split("/").collect();

        let unique_id: u8 = rand::thread_rng().gen_range(0..255);
        let mut file_ext_bytes: Vec<u8> = file_ext[1].as_bytes().to_vec();
        if file_ext_bytes.len() < 4{
            file_ext_bytes.push(32);
        }

        let f_ext: &[u8] = file_ext_bytes.as_ref();
        let udp_socket = NatSetGet::holepunch(nat_socket.clone(),  arc_socket.clone(), holepunch_code.clone() ).await;
        std::thread::sleep(std::time::Duration::from_secs(10));

        let chunks_iterator: Vec<&[u8]> = file_vector.chunks(chunk_size.try_into().unwrap()).collect();
        let chunks_cnt = chunks_iterator.len();
        println!("Chunk_cnt {:?}", chunks_cnt);
        for (chunk_index, chunk) in chunks_iterator.iter().enumerate() {
        
            if chunks_cnt as u32 == chunk_index as u32 + 1 as u32 {
                eof_flag = 1 as u8;
            } 

            let mut header_init: Vec<u8> = (&mut [

                ((unique_id) & 0xff) as u8,              

                ((eof_flag) & 0xff) as u8,
                ((chunk_size >> 8) & 0xff) as u8,
                (chunk_size & 0xff) as u8,

                ((chunk_offset >> 24) & 0xff) as u8,
                ((chunk_offset >> 16) & 0xff) as u8,
                ((chunk_offset >> 8) & 0xff) as u8,
                (chunk_offset & 0xff) as u8,

                ((chunk_index >> 24) & 0xff) as u8,
                ((chunk_index >> 16) & 0xff) as u8,
                ((chunk_index >> 8) & 0xff) as u8,
                (chunk_index & 0xff) as u8,

                ((chunks_cnt >> 24) & 0xff) as u8,
                ((chunks_cnt >> 16) & 0xff) as u8,
                ((chunks_cnt >> 8) & 0xff) as u8,
                (chunks_cnt & 0xff) as u8,

                (f_ext[0] & 0xff) as u8,
                (f_ext[1] & 0xff) as u8,
                (f_ext[2] & 0xff) as u8,
                (f_ext[3] & 0xff) as u8, 

            ]).to_vec(); 

            let mut transfer_data: Vec<u8> = Vec::new();
            transfer_data.append(&mut header_init);
            transfer_data.append(&mut chunk.to_vec() );
    
                let is_send = udp_socket.send(&transfer_data).await;
                match is_send {
                    Ok(_) => {
                        transfer_data_storage.insert(chunk_index as u32, transfer_data.clone());
                        }
                    Err(e) => { println!("Not send, {:?}", e)}
                }      
            chunk_offset += chunk_size;
        }

        let mut resend_buffer = [0u8; 4];
        let duration = std::time::Duration::from_secs(90);    
        loop {
            match tokio::time::timeout(duration, udp_socket.recv(&mut resend_buffer)).await {
                Ok(_i) => {
                        //Transfer Finished 
                        if resend_buffer == [255, 255, 255, 255] {
                            println!("Exit. Resend Transferer Complete");
                            break;
                        }
                    
                        let packet_id = u32::from_be_bytes(<[u8; 4]>::try_from(resend_buffer).expect("Error"));
                        println!("Resend data - packet id, {:?}", packet_id);
                        let resend_package = transfer_data_storage.get(&packet_id).unwrap();
                        let is_send = udp_socket.send(resend_package).await;
                        match is_send {
                            Ok(_) => {
                                println!("Transferred Missing Packed: {:?}", packet_id);
                                }
                            Err(e) => { println!("Not send, {:?}", e)}
                        }
                    }
                Err(el) => {
                    eprintln!("No activity for 90sec {:?}", el.to_string());
                    break;
                }
            }
        }
        drop(udp_socket);
    }     
}
