pub mod route_hander {

    use dirs::home_dir;
    use futures::StreamExt;
    use std::fs;
    use std::{collections::HashMap, sync::Arc};
    use tokio::sync::{mpsc, RwLock};
    use tokio_stream::wrappers::UnboundedReceiverStream;
    use warp::ws::{Message, WebSocket};
    use warp::Filter;
    use bytes::{Buf, BufMut};
    use futures::TryStreamExt;
    use std::str;
    use serde::{Serialize, Deserialize};
    use warp::{
        multipart::FormData,
        Rejection, Reply,
    };
    
    use crate::socket_client::process_client;
    use crate::route::routers::StoreObj;
    use crate::socket_server::process_server;
    extern crate num_cpus;
    
    static NEXT_USERID: std::sync::atomic::AtomicUsize = std::sync::atomic::AtomicUsize::new(10);
    type Users = Arc<RwLock<HashMap<String, mpsc::UnboundedSender<Result<Message, warp::Error>>>>>;
    type SecretCode = Arc<RwLock<HashMap<usize, String >>>;
    
    
        #[derive(Serialize, Deserialize, Debug)]
        struct FileItem {
            file: Vec<u8>,
            file_type: String,
            connection_code: String,
        }
    
        #[derive(Debug, Deserialize, Serialize, Clone)]
        pub struct SingleItem {
            name: String,
            quantity: i32,
        }
    
        #[derive(Debug, Deserialize, Serialize, Clone)]
        pub struct PeerItem {
            peer: String,
            address: String,
        }
    
        pub async fn insert_peer_item(item: PeerItem, store: StoreObj) -> Result<impl warp::Reply, warp::Rejection> {
            store.peer_list.write().await.insert(item.peer, item.address);
            Ok(warp::reply::with_status(
                "Items added to list",
                warp::http::StatusCode::ACCEPTED,
            ))
        }
    
        pub async fn get_peer_item(store: StoreObj, mut form: FormData) -> Result<impl warp::Reply, warp::Rejection> {
           
            let result_json: warp::reply::Json = if let Ok(Some(part)) = form.try_next().await {
                let mut data = Vec::new();
                let mut stream = part.stream();    
    
                while let Ok(Some(mut chunk)) = stream.try_next().await {
                    let slice: &[u8] = chunk.chunk();
                    data.extend_from_slice(slice);
                    chunk.advance(slice.len());
                }

                let mut result = HashMap::new();
                let r = store.peer_list.read().await;
                for (key,value) in r.iter() {
                    if key == &str::from_utf8(&data.clone()).unwrap().to_string()
                    {
                        result.insert(key, value);
                    }
                }
                println!("Result {:?}", result);
                warp::reply::json(
                    &result
                )
            }  else {
                let res:HashMap<String, String> = HashMap::new();
                warp::reply::json(
                    &res
                )
            };
            Ok(result_json)
        }
    
        pub async fn get_file_item(store: StoreObj, form: FormData) -> Result<impl warp::Reply, warp::Rejection> {
    
            let mut parts = form.try_collect::<Vec<_>>().await.unwrap();
            let mut get_part = |name: &str| {
                parts
                .iter()
                .position(|part| part.name() == name)
                .map(|p| parts.swap_remove(p))
                .ok_or(format!("{} part not found", name))
            };
            let peer = get_part("peer").unwrap();
    
            let peer_content = peer
            .stream()
            .try_fold(Vec::new(), |mut vec, data| {
                vec.put(data);
                async move { Ok(vec) }
            })
            .await
            .map_err(|e| {
                eprintln!("reading file error: {}", e);
                warp::reject::reject()
            })?;
    
            let mut result = HashMap::new();
            let r = store.file_list.read().await;
            for (key,value) in r.iter() {
                if key == &str::from_utf8(&peer_content).unwrap().to_string()
                {
                    result.insert(key, value);
                }
            }
            Ok(warp::reply::json(
                &result
            ))
            
        }
    
        pub async fn get_peer_list(store: StoreObj) -> Result<impl warp::Reply, warp::Rejection> {
            let mut result = HashMap::new();
            let r = store.peer_list.read().await;
            for (key,value) in r.iter() {
                result.insert(key, value);
            }
            Ok(warp::reply::json(
                &result
            ))
        }
    
        pub async fn get_peer_file_list(store: StoreObj) -> Result<impl warp::Reply, warp::Rejection> {
            let mut result = HashMap::new();
            let r = store.file_list.read().await;
            for (key,value) in r.iter() {
                result.insert(key, value);
            }
            Ok(warp::reply::json(
                &result
            ))
        }
    
        pub async fn insert_test_item(item: SingleItem, store: StoreObj) -> Result<impl warp::Reply, warp::Rejection> {
            store.item_list.write().await.insert(item.name, item.quantity);
            Ok(warp::reply::with_status(
                "Items added to list",
                warp::http::StatusCode::ACCEPTED,
            ))
        }
    
        pub async fn get_test_item_list(store: StoreObj) -> Result<impl warp::Reply, warp::Rejection> {
            let mut result = HashMap::new();
            let r = store.item_list.read().await;
            for (key,value) in r.iter() {
                result.insert(key, value);
            }
            Ok(warp::reply::json(
                &result
            ))
        }
        
        pub async fn get_file_list() -> Result<impl Reply, Rejection> {
            let mut result: Vec<String> = Vec::new();
            let paths =  fs::read_dir(home_dir().unwrap()).unwrap();        
            for path in paths {
                match path {
                    Ok(item) => {
                        if item.file_name().to_string_lossy().to_string().starts_with("rust_udp-"){
                            result.push(item.file_name().to_string_lossy().to_string())
                        }  
                    }
                    Err(_) => {
                        println!("{:?} cannot be accessed", path);
                        continue;
                    }
                };
            } 
            Ok(warp::reply::json(
                &result
            ))
        }
    
        pub async fn upload(mut form: FormData) -> Result<impl Reply, Rejection> {

            while let Ok(Some(part)) = form.try_next().await {
                let file_name = match part.filename() {
                    Some(file_name) => file_name.to_string(),
                    None => return Ok("Nonw".to_string()),
                };

                let content_type = match part.content_type() {
                    Some(content_type) => content_type.to_string(),
                    None => return Ok("None".to_string()),
                };
    
                println!("Receiving file: {}", file_name);
        
                let mut data = Vec::new();
                let mut stream = part.stream();
                
                while let Ok(Some(mut chunk)) = stream.try_next().await {
                    let slice: &[u8] = chunk.chunk();
                    data.extend_from_slice(slice);
                    chunk.advance(slice.len());
                }
    
                let path = home_dir().unwrap();
                let path_str = path.to_str().unwrap();
                println!("{:?}, {:?}", file_name, content_type);
                let file_name = format!("{}/{}.{}", path_str, uuid::Uuid::new_v4().to_string().as_str() , file_name);
                let _z = tokio::fs::write(&file_name, data).await.map_err(|e| {
                    eprint!("error writing file: {}", e);
                    warp::reject::reject()
                }).unwrap();
            }
            Ok("success".to_string())

        }
    
        pub async fn ws_connect(ws: WebSocket, users: Users, _secret_code: SecretCode) {
    
            let my_id = NEXT_USERID.fetch_add(std::convert::TryInto::try_into(12).unwrap(), std::sync::atomic::Ordering::Relaxed);
            println!("Welcome User {}", my_id);
    
            let (ws_out, mut ws_in) = ws.split();
            let (mpsc_us, mpsc_ur) = mpsc::unbounded_channel();
            let tx_cloned = mpsc_us.clone();
            
            let secret_code_for_connection: String = rand::Rng::sample_iter(rand::thread_rng(), &rand::distributions::Alphanumeric)
            .take(7)
            .map(char::from)
            .collect();
    
            let secret_code_for_connection_cloned = secret_code_for_connection.clone();
            tokio::task::spawn(async move {
                process_server::UdpServer::start_server(tx_cloned, secret_code_for_connection_cloned).await;
            });
        
            let stream_rx = UnboundedReceiverStream::new(mpsc_ur);
    
            tokio::spawn(
                stream_rx.forward(ws_out)
            );
            
            users.write().await.insert(secret_code_for_connection.clone(), mpsc_us.clone());
            
            let connection_code: warp::ws::Message = Message::binary(secret_code_for_connection.clone());
            let connection_code_message: Result<Message, warp::Error> = Ok(connection_code);
            let _send_result = mpsc_us.send(connection_code_message);
    
            while let Some(result) = ws_in.next().await {
        
                let result_match = match result {
                    Ok(_) => Ok(result.as_ref().unwrap()),
                    Err(_) => Err("Error"),
                };

                let data_set: FileItem = serde_json::from_slice(result_match.unwrap().as_bytes()).unwrap();
         
                let user_lock = users.read().await;
                println!("code {:?}", data_set.connection_code);
                let (_key,sender) = user_lock.get_key_value(& data_set.connection_code).unwrap();
                
                let bin_message: warp::ws::Message = warp::ws::Message::binary("Receiver Browser - Loader");
                let message: Result<warp::ws::Message, warp::Error> = Ok(bin_message);
                let _ = sender.send(message);
    
                let handler = tokio::task::spawn(async move {
                    let nat = crate::connector::nat::G_MAP_CODE_SOCKET.get().lock().await;
                    let udp_socket = nat.get("key-nat-translator").unwrap();
                    let udp_socket_cloned =  udp_socket.clone();  
                    process_client::start_client( data_set.file, data_set.file_type, data_set.connection_code, udp_socket_cloned ).await;
                });
                
                let _handler_await = handler.await;
                let message_complete: warp::ws::Message = Message::binary("File transfer complete. Request new code for next transfer");
                mpsc_us.send(Ok(message_complete)).expect("Failed to send message");
            }
    
            //Disconnect
            //disconnect(my_id, &users, &secret_code).await;
        }
    
        
        /* 
         async fn disconnect(my_id: usize, users: &Users, code: &SecretCode) {
         }
        */
    
        pub fn post_json() -> impl Filter<Extract = (SingleItem,), Error = warp::Rejection> + Clone {
            warp::body::content_length_limit(1024 * 16).and(warp::body::json())
        }
    }
    
    