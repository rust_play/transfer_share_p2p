pub mod nat {  
    
    use std::str::FromStr;
    use std::sync::Arc;
    use tokio::net::UdpSocket;
    use std::{
        collections::HashMap,
        net::*,
    };

    use std::net::SocketAddr;
    use state::Storage;
    pub static G_MAP_CODE_SOCKET: Storage<futures::lock::Mutex<HashMap<String, Arc<UdpSocket>>>> = Storage::new();

    pub struct NatSetGet {
        pub socket: Arc<UdpSocket>,
        map: HashMap<[u8; 15], SocketAddr>,
    }

    impl NatSetGet {
        
        pub fn new(socket: Arc<UdpSocket>) -> NatSetGet {
            let mut initial_map: HashMap<String,Arc<UdpSocket>> = HashMap::new();
            initial_map.insert("key-nat-translator".into(), socket.clone());
            G_MAP_CODE_SOCKET.set(futures::lock::Mutex::new(initial_map));
             NatSetGet {
                socket,
                map: HashMap::new(),
            } 
        }
         
        pub fn get_socket(&mut self) -> Arc<UdpSocket> {
            return self.socket.clone();
        }  
    
        pub async fn holepunch(nat_socket: Arc<UdpSocket>, socket: Arc<UdpSocket>, connection_code: String)  -> Arc<UdpSocket> {

            //Send to NAT Key - Address
            let key_secret = connection_code.as_bytes();
            let mut key_buf = [0 as u8; 15];
             for i in 0..key_secret.len().min(15) {
                 key_buf[i] = key_secret[i];
            }
            
            socket.connect(nat_socket.local_addr().unwrap() ).await.unwrap();
            socket.send(&key_buf).await.unwrap();
            socket.recv(&mut key_buf).await.expect("Can not receive from connector");     

            let mut key_buf_vec = Vec::from(key_buf);
            key_buf_vec = key_buf_vec.into_iter().filter(|x| *x != 0).collect();
            let bind_addr = String::from_utf8_lossy(key_buf_vec.as_slice()).to_string();
            
            println!(
                "HolePunch Address Mapping {} (source address) -> {} (destination address)",
                bind_addr,
                socket.local_addr().unwrap(),
            ) ;
        
            socket.connect(SocketAddrV4::from_str(bind_addr.as_str()).unwrap()).await.unwrap();
            return socket
        }

        pub async fn start_nat_listener(&mut self) {

             let nat = self.get_socket(); 
             println!("Start NAT LOCAL {:?}", nat.local_addr().unwrap());      
             let mut buf_key = [0 as u8; 15];
             let mut bytes: &[u8];
             let mut addr_buf = [0 as u8; 15];
             let mut partner_buf = [0 as u8; 15];

             loop {
                 let (_l, incoming_addr) = nat.recv_from(&mut buf_key).await.expect("read error");
                 println!("Awaiting for message {:?}", std::str::from_utf8(&buf_key).unwrap());
                 if self.map.contains_key(&buf_key) {
     
                     let partner_address = self.map.get(&buf_key).unwrap();

                     // connection
                     bytes = incoming_addr.to_string().bytes().collect::<Vec<u8>>().leak();
                    
                     for i in 0..bytes.len().min(15) {
                         addr_buf[i] = bytes[i];
                     }
     
                     bytes = partner_address.to_string().bytes().collect::<Vec<u8>>().leak();
                     for i in 0..bytes.len().min(15) {
                         partner_buf[i] = bytes[i];
                     }


                     if nat.send_to(&addr_buf, partner_address).await.is_ok() && 
                        nat.send_to(&partner_buf, incoming_addr).await.is_ok() {
                         println!("Exchange completed");
                     }
                     //self.map.remove(&buf);
                 } else {
                     self.map.insert(buf_key, incoming_addr);
                 }  
            }
        }

        pub async fn start_nat() -> Arc<UdpSocket> {
            
            let bind_addr = (Ipv4Addr::from(0 as u32), 0); //NAT SHOULD BE FIXED
            let nat_listener = UdpSocket::bind(&bind_addr).await.expect("can not create socket");
            let arc_nat_listener = Arc::new(nat_listener);
            let arc_nat_listener_cloned = arc_nat_listener.clone();
            let mut nat = self::NatSetGet::new( arc_nat_listener);

            tokio::task::spawn(async move {
                nat.start_nat_listener().await; 
            });
            return arc_nat_listener_cloned; 
        }

    }   
    
}