pub mod packet {

    use std::collections::HashMap;
    use std::io::prelude::*;
    use std::time::SystemTime;
    use byteorder::{BigEndian, ReadBytesExt};
    
    pub struct DataPacket {
        pub unique_id: u8,
        pub is_eof: u8,
        pub chunk_size: u16,
        pub chunk_offset: u32,
        pub chunk_id: u32,
        pub chunk_cnt: u32,
        pub chunk_data: Vec<u8>,
        pub file_ext_0: u8,
        pub file_ext_1: u8,
        pub file_ext_2: u8,
        pub file_ext_3: u8,
    }
    
    impl DataPacket {
        pub fn new<R: BufRead + Seek> (reader: &mut R) -> DataPacket {
            
            let unique_id = reader.read_u8().unwrap();
            let is_eof = reader.read_u8().unwrap();
            let chunk_size = reader.read_u16::<BigEndian>().unwrap();
            let chunk_offset = reader.read_u32::<BigEndian>().unwrap();
            let chunk_id = reader.read_u32::<BigEndian>().unwrap(); 
            let chunk_cnt = reader.read_u32::<BigEndian>().unwrap(); 
            let file_ext_0 = reader.read_u8().unwrap();
            let file_ext_1 = reader.read_u8().unwrap();
            let file_ext_2 = reader.read_u8().unwrap(); 
            let file_ext_3 = reader.read_u8().unwrap(); 
            let mut chunk_data = Vec::new();
            let _ = reader.take(chunk_size as u64).read_to_end(&mut chunk_data);
            DataPacket { unique_id: unique_id, is_eof: is_eof, chunk_size: chunk_size, chunk_offset: chunk_offset,
                     chunk_id: chunk_id, chunk_cnt: chunk_cnt, file_ext_0, file_ext_1, file_ext_2, file_ext_3 , chunk_data: chunk_data }
        }
    }
    
    pub struct FileList {
        pub messages: Vec<File>,
    }
    
    impl FileList {
        pub fn new() -> FileList {
            FileList { 
                messages: vec![], 
            }
        }
        pub fn add_message(&mut self, message: File) {
            self.messages.push(message);
        }
    }
    
    pub struct File {
        pub unique_id: u8,
        pub complete: bool,
        pub start: SystemTime,
        pub last_received_at: SystemTime, 
        pub packets: Vec<DataPacket>,
        pub packets_received: u32,
        eof: u32,
    }
    
    impl File {
    
        pub fn new(packet_unique_id: u8) -> File {
    
            File { 
                unique_id: packet_unique_id,
                complete: false,
                start: SystemTime::now(), 
                last_received_at: SystemTime::now(), 
                packets: vec![], 
                packets_received: 0, 
                eof: 0,
            }
        }
    
        pub fn add_packet(&mut self, packet: DataPacket, drop_packet: u32) {
    
            if self.unique_id == packet.unique_id {
                if packet.chunk_id == drop_packet {
                    println!("Goto: Packet/File/AddPacket(). Packet Dropped (Simulation - OFF) {:?}, {:?}" , drop_packet, packet.chunk_id);
                    //return; //Force Packet Drop
                 }
                
                self.packets_received += packet.chunk_size as u32;
                if packet.is_eof == 1 { 
                    self.eof = packet.chunk_offset + packet.chunk_size as u32;
                    println!("Packet with EOF Flag - Chunk ID , {:?}, {:?}", self.eof, packet.chunk_id);
                }
    
                self.last_received_at = SystemTime::now();
                self.packets.push(packet);
                if self.eof == self.packets_received {
                    self.complete = true;
                    println!("File Completed, {:?}", self.eof );
                }
            }
        }
    
        pub fn get_uuid(&mut self) -> u8{
            return self.unique_id;
        }
    
        pub fn get_missing_packet_list(&self) -> Vec<u32> {
            let mut missing_packet = vec![];
            let mut trans: HashMap<u32, u32> = HashMap::new();
    
            if self.packets.iter().count() > 0 {
                let first = self.packets.first().unwrap();
                for (_i, packet) in self.packets.iter().enumerate() {
                    trans.insert(packet.chunk_id, packet.chunk_id);
                }
    
                for i in 0..first.chunk_cnt {
                    if !trans.contains_key(&i) {
                        missing_packet.push(i)
                    }
                }
            }
            return missing_packet;
        }
    
        pub fn sort(&mut self) {
            self.packets.sort_by(|a, b| a.chunk_offset.cmp(&b.chunk_offset));
        }
    }
}
    